import 'package:flutter/material.dart';
import 'widgets/flower_logo.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  final color = Colors.green;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.grey[50],
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              FlowerLogo(220, color),
              Column(
                children: [
                  settingCard(
                      icon: Icons.network_cell,
                      text: "Use mobile data",
                      value: false),
                  settingCard(
                      icon: Icons.circle, text: "Use dark mode", value: true),
                  settingCard(
                      icon: Icons.cloud, text: "Use cloud storage", value: true)
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget settingCard(
      {required IconData icon, required String text, required bool value}) {
    return Card(
      margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 12.0),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: [
                  Icon(
                    icon,
                    size: 30.0,
                  ),
                  const SizedBox(
                    width: 10.0,
                  ),
                  Text(
                    text,
                    textAlign: TextAlign.left,
                    style: const TextStyle(
                      fontSize: 18.0,
                    ),
                  ),
                ],
              ),
              Switch(
                  value: value,
                  activeColor: color,
                  onChanged: (bool newValue) {
                    // ignore: avoid_print
                    print('Why does this switch not switch values...?');
                  }),
            ]),
      ),
    );
  }
}
